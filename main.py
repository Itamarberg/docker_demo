"""
Exercise: Docker Ex4
Author: Itamar Bergfreund
Date 15. March 2020
"""

import requests
import os

# urls = ['http://www.reddit.com', 'http://www.wikipedia.org', 'http://www.itc.tech']

for url in os.environ['urls'].split(','):
    print(requests.get(url).text)
